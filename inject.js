// const ipcRenderer = require('electron').ipcRenderer
const {ipcRenderer} = require('electron')
//const webview = document.getElementById('view')

__myYoutubeTools = {
  getMP: function () {
    return document.getElementsByTagName('video')[0]
  },
  classChangeEvent: function () {
    $('#changeClass').on('fullScreen', function () {
      ipcRenderer.send('play', 'play')
    })
    $('#changeClass').on('smallScreen', function () {
      ipcRenderer.send('stop', 'stop')
    })
  },
  pauseVideo: function () {
    // ipcRenderer.send('stop', 'stop')
    var t = __myYoutubeTools
    t.clearPNSTo()
    t.getMP().pauseVideo()
    t.isPaused = true
  },
  playVideo: function () {
    var t = __myYoutubeTools
    t.clearPNSTo()
    //t.getMP().play()
    t.isPaused = false
  },
  clearPNSTo: function () {
    var t = __myYoutubeTools
    try {clearTimeout(t.toPNS)} catch (x) {}
  }
  , isPaused: true
  , toPNS: null
  , playNpauseVideo: function () {
    // debugger
    var t = __myYoutubeTools
    if (t.isPaused) {
      t.playVideo()
    } else {
      t.pauseVideo()
    }
    t.toPNS = setTimeout('__myYoutubeTools.playNpauseVideo()', 1000)
  }
}

/* Get the documentElement (<html>) to display the page in fullscreen */

