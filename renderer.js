// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
var ById = function (id) {
  return document.getElementById(id)
}
// var jsonfile = require('jsonfile')
// var favicon = require('favicon-getter').default
// var path = require('path')
// var uuid = require('uuid')
// var screenfull = require('screenfull')
// const electron = require('electron');
// const Menu = electron.Menu || electron.remote.Menu;
// const Browserwindow = electron.BrowserWindow || electron.remote.BrowserWindow;
// var bookmarks = path.join(__dirname, 'bookmarks.json')
const webview = document.getElementById('view')
const { ipcRenderer, remote } = require('electron');

// Fetch the preferences object
const preferences = ipcRenderer.sendSync('getPreferences');
document.getElementById('view').setAttribute("src", preferences.settings.setting_url);

// When everything is ready, trigger the events without problems
// webview.addEventListener('dom-ready', function () {
//   console.log('DOM-Ready, triggering events !')
//   // Aler the scripts src of the website from the <webview>
//   // webview.send("request");
//
//   // alert-something
// //  webview.send('alert-something', 'Hey, i\'m alerting this.')
//
//   // change-text-element manipulating the DOM
//   // webview.send('change-text-element', {
//   //   id: 'myelementID',
//   //   text: 'My text'
//   // })
// })

// Process the data from the webview
// webview.addEventListener('ipc-message', function (event) {
//   console.log('from CSS TV Browser')
//   mainWindow.maximize()
//   // console.log(event);
//   // console.info(event.channel);
// })
var back = ById('back'),
  forward = ById('forward'),
  refresh = ById('refresh'),
  omni = ById('url'),
  dev = ById('console'),
  fave = ById('fave'),
  list = ById('list'),
  popup = ById('fave-popup'),
  view = ById('view')
onload = function () {
 // var webview = document.querySelector('webview')
 //  $('#bunny').on('classChange', function () {
 //    $('#bunny')[0].play()
 //    $('#bunny')[0].webkitRequestFullScreen()
 //  })

  webview.addEventListener('dom-ready', function () {
    //webview.webkitRequestFullScreen()
    //webview.executeJavaScript('__myYoutubeTools.pauseVideo()')
    webview.executeJavaScript('__myYoutubeTools.classChangeEvent()')
  })
  // document.querySelector('#pause').onclick = function () {
  //  // webview.executeJavaScript('__myYoutubeTools.pauseVideo()')
  // }
  // document.querySelector('#play').onclick = function () {
  //  // webview.webkitRequestFullScreen()
  //   webview.executeJavaScript('__myYoutubeTools.playVideo()')
  // }
  // document.querySelector('#pauseNstart').onclick = function () {
  //   //webview.executeJavaScript('__myYoutubeTools.playNpauseVideo()')
  // }
}

function reloadView () {
  view.reload()
}

function backView () {
  view.goBack()
}

function forwardView () {
  view.goForward()
}
function updateURL (event) {
  if (event.keyCode === 13) {
    omni.blur()
    let val = omni.value
    let https = val.slice(0, 8).toLowerCase()
    let http = val.slice(0, 7).toLowerCase()
    if (https === 'https://') {
      view.loadURL(val)
    } else if (http === 'http://') {
      view.loadURL(val)
    } else {
      view.loadURL('http://' + val)
    }
  }
}

function addEvent () {

}

var Bookmark = function (id, url, faviconUrl, title) {
  this.id = id
  this.url = url
  this.icon = faviconUrl
  this.title = title
}

Bookmark.prototype.ELEMENT = function () {
  var a_tag = document.createElement('a')
  a_tag.href = this.url
  a_tag.className = 'link'
  a_tag.textContent = this.title
  var favimage = document.createElement('img')
  favimage.src = this.icon
  favimage.className = 'favicon'
  a_tag.insertBefore(favimage, a_tag.childNodes[0])
  return a_tag
}

function addBookmark () {
  let url = view.src
  let title = view.getTitle()
  favicon(url).then(function (fav) {
    let book = new Bookmark(uuid.v1(), url, fav, title)
    jsonfile.readFile(bookmarks, function (err, curr) {
      curr.push(book)
      jsonfile.writeFile(bookmarks, curr, function (err) {
      })
    })
  })
}

function openPopUp (event) {
  let state = popup.getAttribute('data-state')
  if (state === 'closed') {
    popup.innerHTML = ''
    jsonfile.readFile(bookmarks, function (err, obj) {
      if (obj.length !== 0) {
        for (var i = 0; i < obj.length; i++) {
          let url = obj[i].url
          let icon = obj[i].icon
          let id = obj[i].id
          let title = obj[i].title
          let bookmark = new Bookmark(id, url, icon, title)
          let el = bookmark.ELEMENT()
          popup.appendChild(el)
        }
      }
      popup.style.display = 'block'
      popup.setAttribute('data-state', 'open')
    })
  } else {
    popup.style.display = 'none'
    popup.setAttribute('data-state', 'closed')
  }
}

function handleUrl (event) {
  if (event.target.className === 'link') {
    event.preventDefault()
    view.loadURL(event.target.href)
  } else if (event.target.className === 'favicon') {
    event.preventDefault()
    view.loadURL(event.target.parentElement.href)
  }
}

function handleDevtools () {
  if (view.isDevToolsOpened()) {
    view.closeDevTools()
  } else {
    view.openDevTools()
  }
}

// function updateNav (event) {
//   omni.value = view.src
// }

// refresh.addEventListener('click', reloadView)
// back.addEventListener('click', backView)
// forward.addEventListener('click', forwardView)
// omni.addEventListener('keydown', updateURL)
// fave.addEventListener('click', addBookmark)
// list.addEventListener('click', openPopUp)
// popup.addEventListener('click', handleUrl)
// dev.addEventListener('click', handleDevtools)
//view.addEventListener('did-finish-load', updateNav)
// https://github.com/hokein/electron-sample-apps/blob/master/webview/browser/browser.js#L5
// To Do add dev tools open ✔️
// update url ✔️
// add bookmark by pressing button ✔️
// load all bookmarks when list is clicked ✔️

// To Do / Continue
// Feedback when loading
// Feedback with favorite icon to show that bookmark is not-added/added/already-added
// Tabs !:@
// Option to remove bookmarks.


