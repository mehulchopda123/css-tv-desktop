const electron = require('electron')
// Enable live reload for all the files inside your project directory
require('electron-reload')(__dirname)
const os = require('os');
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const path = require('path')
const url = require('url')
const ipc = require('electron').ipcMain
const {globalShortcut}=require('electron')
const ElectronPreferences = require('electron-preferences');
const preferences = new ElectronPreferences({
  /**
   * Where should preferences be saved?
   */
  'dataStore': path.resolve(app.getPath('userData'), 'preferences.json'),
  /**
   * Default values.
   */
  'defaults': {
    // 'notes': {
    //   'folder': path.resolve(os.homedir(), 'Notes')
    // },
    'markdown': {
      'auto_format_links': true,
      'show_gutter': false
    },
    'preview': {
      'show': false
    },
    'drawer': {
      'show': false
    }
  },
  /**
   * If the `onLoad` method is specified, this function will be called immediately after
   * preferences are loaded for the first time. The return value of this method will be stored as the
   * preferences object.
   */
  'onLoad': (preferences) => {
  // ...
  return preferences;
  },
/**
 * The preferences window is divided into sections. Each section has a label, an icon, and one or
 * more fields associated with it. Each section should also be given a unique ID.
 */
'sections': [
  {
    'id': 'settings',
    'label': 'Settings',
    /**
     * See the list of available icons below.
     */
    'icon': 'single-01',
    'form': {
      'groups': [
        {
          /**
           * Group heading is optional.
           */
          'label': 'Settings',
          'fields': [
            {
              'label': 'height',
              'key': 'setting_height',
              'type': 'text',
              /**
               * Optional text to be displayed beneath the field.
               */
              'help': 'Please assign your setting for browser height'
            },
            {
              'label': 'Default Url',
              'key': 'setting_url',
              'type': 'text',
              'help': 'Please assign your setting for browser default url '
            },
            {
              'label': 'Add Proxy',
              'key': 'setting_proxy',
              'type': 'text',
              'help': 'Please add your proxy here '
            },
          ]
        }
      ]
    }
  },

]
});

//const { ipcMain } = require('electron')
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let screen
//app.commandLine.appendSwitch('--autoplay-policy','no-user-gesture-required')

function createWindow () {
  // Create the browser window.
  screen = require('electron').screen
 //check whether application is running for the first time
  var cmd = process.argv[1];
  if (cmd == '--squirrel-firstrun') {
    // Running for the first time.
    //By Default set height and url of the browserwindow
     preferences.value('settings.setting_height', '180');
     preferences.value('settings.setting_url', 'http://mehulchopda.com/privacy-policy');
  }
    preferences.value('settings.setting_height', '180');
    preferences.value('settings.setting_url', 'http://mehulchopda.com/privacy-policy');
  const height_test = parseInt(preferences.value('settings.setting_height'));
  mainWindow = new BrowserWindow({
    x: 0,
    y: 0,
    frame: false,
    //alwaysOnTop: true,
    width: screen.getPrimaryDisplay().size.width,
    height: height_test,
    // useContentSize:true,
  })
  mainWindow.setMenu(null)
  // and load the index.html of the app.
  // mainWindow.loadURL(url.format({
  //   pathname: path.join(__dirname, 'index.html'),
  //   protocol: 'file:',
  //   slashes: true
  // }))
     const setting_proxy = preferences.value('settings.setting_proxy');
      if(setting_proxy){
          mainWindow.webContents.session.setProxy({proxyRules: "socks5:"+"//"+setting_proxy}, function () {
            mainWindow.loadURL(url.format({
                pathname: path.join(__dirname, 'index.html'),
                protocol: 'file:',
                slashes: true
            }));
        });
      }
      else{
          mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, 'index.html'),
            protocol: 'file:',
            slashes: true
          }))
      }
    //set proxy
  // mainWindow.maximize()
  // mainWindow.setKiosk(true)
  //mainWindow.setFullScreen(true)
 // mainWindow.setSkipTaskbar(true)
  // Open the DevTools.
    //
  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
  ipc.on('play', function (event, arg) {
    mainWindow.setKiosk(true)
    //mainWindow.maximize()
  })
  ipc.on('stop', function (event, arg) {
   // mainWindow.minimize()
    mainWindow.setKiosk(false)
  })
  const ret = globalShortcut.register('CommandOrControl+L', () => {
    preferences.show();
  })
  if (!ret) {
    console.log('registration failed')
  }
  //open devtools console
    const ret1 = globalShortcut.register('CommandOrControl+D', () => {
        mainWindow.webContents.openDevTools();
})
    if (!ret1) {
        console.log('registration failed')
    }
  // refresh the webview
    const refresh = globalShortcut.register('CommandOrControl+R', () => {
        mainWindow.reload();
})
    if (!refresh) {
        console.log('registration failed')
    }
  // Check whether a shortcut is registered.
  // console.log(globalShortcut.isRegistered('CommandOrControl+L'))

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)
// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})
app.commandLine.appendSwitch('--disable-touch-adjustment')

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.